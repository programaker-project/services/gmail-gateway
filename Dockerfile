# Build binary
FROM golang:alpine as builder

RUN apk add --no-cache git

RUN go get -u google.golang.org/api/gmail/v1 && \
  go get -u golang.org/x/oauth2/google

WORKDIR /go/src/gitlab.com/programaker-project/services/gmail-gateway
COPY src /go/src/gitlab.com/programaker-project/services/gmail-gateway/
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o gmail-gateway .

# Build final image
FROM alpine as runner
WORKDIR /app/
COPY --from=builder /go/src/gitlab.com/programaker-project/services/gmail-gateway/gmail-gateway .

EXPOSE 8080

CMD ["./gmail-gateway"]
