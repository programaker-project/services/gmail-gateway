package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"google.golang.org/api/gmail/v1"
)

type Response struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
}

func handle_mail(w http.ResponseWriter, req *http.Request, srv *gmail.Service) {
	decoder := json.NewDecoder(req.Body)
	var mail_data MailData
	response := Response{Success: true, Message: "ok"}

	err := decoder.Decode(&mail_data)
	if err != nil {
		response = Response{
			Success: false,
			Message: err.Error(),
		}
	} else {
		err = Send(srv, mail_data)
		if err != nil {
			response = Response{
				Success: false,
				Message: err.Error(),
			}
		}
	}

	json.NewEncoder(w).Encode(response)
}

func main() {
	srv := Prepare()

	http.HandleFunc("/mail/send", func(w http.ResponseWriter, req *http.Request) {
		handle_mail(w, req, srv)
	})
	fmt.Fprintf(os.Stderr, "Ready\n")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
