package main

type MailData struct {
	Sender      string `json:"sender"`
	Receiver    string `json:"receiver"`
	Subject     string `json:"subject"`
	Message     string `json:"message"`
	ContentType string `json:"content_type"`
}
